var Display = new function() {
	this.scale = function(w,h) {
		$("#display-canvas").css("");
		if(w < h)
			$("#display-canvas").width("100%");
		else
			$("#display-canvas").height("100%");
	}
	this.resize = function() {
		this.scale($(window).width(),$(window).height());
		this.context.fillStyle = "black";
		this.context.fillRect(0,0,this.canvas.width,this.canvas.height); 
	};
	this.init = function() {
		this.canvas = $("#display-canvas");
		this.context = this.canvas[0].getContext("2d");
		$(window).resize(this.resize);
		this.resize();
	};
};